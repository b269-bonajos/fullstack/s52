import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'

// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Login(){

    const{user, setUser} = useContext(UserContext);

    // to store values of the input fields
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // hook returns a function that lets you navigate to components
    // const navigate = useNavigate();

    function authenticate(e){
        e.preventDefault()

        // Set the email of the authenticated user in the local storage
        // localStorage.setItem('propertyName', value)
        localStorage.setItem('email', email);

        // sets the global user state to have properties obtain from local storage
        setUser({email: localStorage.getItem('email')});

        setEmail('')
        setPassword('');

        // navigate('/')

        alert("Successfully login!")
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.email !== null) ?
        <Navigate to='/courses'/>
        :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="secondary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}






// import {useState, useEffect} from 'react';
// import { Form, Button } from 'react-bootstrap';

// export default function Register() {

//     const [email, setEmail] = useState("");
//     const [password1, setPassword1] = useState("");
//     // to determine whether submit button is enabled or not
//     const [isActive, setIsActive] = useState(false);

// /*
// if(!email && !password1 && !password2 && password1 === password2)
// if ( email !== null && password1 !== null && password2 !== null && password1 === password2)
// */


//     useEffect(() => {
//         if(( email !== "" && password1 !== "")) {
//             setIsActive(true);
//         } else {
//             setIsActive(false);
//         };
//     }, [email, password1]);


//     function registerUser(e) {
//         e.preventDefault();

//         setEmail("");
//         setPassword1("");

//         alert("You are logged in!");
//     };

//     return (
//         <Form onSubmit={(e) => registerUser(e)}>
//             <Form.Group controlId="userEmail">
//                 <h1>Login</h1>
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
//                     type="email" 
//                     placeholder="Enter email" 
//                     value={email}
//                     onChange={e => setEmail(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             <Form.Group controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Password" 
//                     value={password1}
//                     onChange={e => setPassword1(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             {isActive ?
//                 <Button variant="primary" type="submit" id="submitBtn">
//                     Submit
//                 </Button>
//                 :
//                 <Button variant="success" type="submit" id="submitBtn" disabled>
//                     Login
//                 </Button>


//             }

//         </Form>
//     )

// }







